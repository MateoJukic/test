﻿using System;
using System.Linq.Expressions;

namespace Zadatak2
{
    /*Napisati program koji sadrži dvije varijable, jednu tipa int, a drugu tipa long u koju će
biti zapisana najveća moguća vrijednost za tip long. Varijablu tipa long treba
pridružiti varijabli tipa int, s tim da se obradi iznimka ako dođe do preljeva
(overflow).
Pomoć: vidjeti “checked” u MSDN 
*/
    class Program
    {
        static void Main(string[] args)
        {
            int num = 10;
            long longy = long.MaxValue;
            try
            {
                num += checked((int)longy);
                Console.WriteLine(num);
            }
            catch(OverflowException e) 
            {
                Console.WriteLine("Preljev");
            }
               
        }
    }
}
