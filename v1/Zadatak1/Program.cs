﻿using System;

namespace Zadatak1
{

    /*
     Napisati program koji upisuje dva cjelobrojna broja i ispisuje rezultat dijeljenja ta dva
broja. Rezultat treba ispisati u sljedećim formatima (Currency, Integer, Scientific,
Fixed-point, General, Number, Hexadecimal).
Prilikom upisa nekog podatka sa tipkovnice, podatak se učitava kao tip string, a ako
nam treba tip int moramo ga pretvoriti uz pomoć ugrađenih metoda.
Pripaziti da se obrade sve iznimke 
*/
    
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Unesite dva cjelobrojna broja:");

            try
            {
                int num1 = int.Parse(Console.ReadLine());
                int num2 = int.Parse(Console.ReadLine());

                float res = num1 / num2;

                Console.WriteLine("Currency format : " + res.ToString("c"));
                Console.WriteLine("Integer format : " + ((int)res).ToString());
                Console.WriteLine("Scientific format : " + res.ToString("e"));
                Console.WriteLine("Fixed-point format : " + res.ToString("f"));
                Console.WriteLine("General format : " + res.ToString("g"));
                Console.WriteLine("Number format : " + res.ToString("n"));
                Console.WriteLine("Hexadecimal format : " + ((int)res).ToString("x"));


            }
            catch (FormatException a)
            {
                Console.WriteLine("Krivi unos.");
            }
            catch (OverflowException a)
            {
                Console.WriteLine("Preveliki broj.");
            }
            catch (DivideByZeroException a)
            {
                Console.WriteLine("Dijeljenje sa 0 nije dozvoljeno.");
            }
        }
    }
}
