﻿using System;
using System.Collections;
using System.Collections.ObjectModel;

namespace Zadatak3
{
    /*Napisati program za bankare koji ima deklariran pobrojani tip podataka u kojem se
nalaze vrste računa (Štednja , Tekući račun, Žiro račun). Unutar programa deklarirati
strukturu BankAccount koja će sadržavati tri varijable, broj računa, iznos na računu i
vrstu računa.
Program treba deklarirati polje struktura BankAccount od 5 elemenata, te napraviti
izbornik koji će imati opcije, upisa novog računa, i ispis svih računa. Za ispis svih
računa koristiti “foreach” iteraciju. */
    class Program
    {
        enum racuni 
        {
            Štednja,
            TekućiRačun,
            ŽiroRačun
        }
        struct BankAccount
        {
            string brojRacuna;
            double iznosRacuna;
            string vrstaRacuna;

            public BankAccount(string brojRacuna, double iznosRacuna, racuni vrstaRacuna) : this()
            {
                this.brojRacuna = brojRacuna;
                this.iznosRacuna = iznosRacuna;
                this.vrstaRacuna = vrstaRacuna.ToString();
            }

            public string toString()
            {
                return "Broj Računa: " + brojRacuna + "\n" + "Iznos Računa: " + iznosRacuna.ToString("c") + "\n" + "Vrsta Računa: " + vrstaRacuna+"\n\n\n";
            }
        }

        static void Main(string[] args)
        {
            Collection<BankAccount> racuni = new Collection<BankAccount>();
            racuni.Add(new BankAccount("123456789", 560.78, (racuni)1));
            racuni.Add(new BankAccount("123222789", 22560.70, (racuni)0));
            racuni.Add(new BankAccount("123221789", 1560.78, (racuni)1));
            racuni.Add(new BankAccount("166456789", 5555, (racuni)2));
            racuni.Add(new BankAccount("123457667", 111.34, (racuni)2));
            

            while (true) 
            {
                Console.WriteLine("Glavni izbornik");
                Console.WriteLine();
                Console.WriteLine("[1] ----- Dodaj račun");
                Console.WriteLine("[2] ----- Ispis računa");
                string input = Console.ReadLine();
                if(input == "1")
                {
                    try
                    {
                        Console.WriteLine("Unos novog računa.");
                        Console.WriteLine("Unesite broj računa!");
                        string brojRacuna = Console.ReadLine();
                        int.Parse(brojRacuna);
                        Console.WriteLine();

                        Console.WriteLine("Unesite iznos na računu!");
                        double iznosRacuna = double.Parse(Console.ReadLine());
                        Console.WriteLine();

                        Console.WriteLine("Odaberite tip računa!");
                        Console.WriteLine("[0] ----- Štednja");
                        Console.WriteLine("[1] ----- Tekući Račun");
                        Console.WriteLine("[2] ----- Žiro Račun");
                        int vrstaRacuna;
                        string i = Console.ReadLine();
                        if (int.Parse(i) < 3 && int.Parse(i) > -1)
                            vrstaRacuna = int.Parse(i);
                        else
                        {
                            Console.WriteLine("Krivi unos");
                            continue;
                        }


                        BankAccount racun = new BankAccount(brojRacuna, iznosRacuna, (racuni)vrstaRacuna);
                        racuni.Add(racun);
                    }
                    catch(FormatException e)
                    {
                        Console.WriteLine("Krivi Unos");
                    }
                    catch (OverflowException e)
                    {
                        Console.WriteLine("Preveliki iznos");
                    }
                }
                else if(input == "2")
                {
                    foreach(BankAccount racun in racuni)
                        Console.WriteLine(racun.toString());
                }
            }
        }
    }
}
