﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba4
{
    class Klijent
    {
        public string ime { get; set; }
        public string prezime { get; set; }
        public string OIB { get; set; }
        public DateTime datumRodenja { get; set; }
        public string adresa { get; set; }
        public char spol { get; set; }

        public Klijent(
                string ime,
                string prezime, string OIB,
                DateTime? datumRodenja, string adresa,
                char spol){

            this.ime = ime;
            this.prezime = prezime;
            this.OIB = OIB;
            this.datumRodenja = (DateTime) datumRodenja;
            this.adresa = adresa;
            this.spol = spol;
        }
    }
}
