﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba4
{
    class BankovniRacun
    {
        public enum VrsteRacuna{
                Štednja,
                TekućiRačun,
                ŽiroRačun
        }

        public string brojRacuna { get; set; }
        public double iznosRacuna { get; set; }
        public string vrstaRacuna { get; set; }
        public Klijent Klijent { get; set; }



        public BankovniRacun(
                string brojRacuna, double iznosRacuna,
                VrsteRacuna vrstaRacuna, Klijent klijent){

            this.brojRacuna = brojRacuna;
            this.iznosRacuna = iznosRacuna;
            this.vrstaRacuna = vrstaRacuna.ToString();
            this.Klijent = klijent;
        }

    }
}
