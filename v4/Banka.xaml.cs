﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Vjezba4.BankovniRacun;

namespace Vjezba4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Banka : Window
    {
        private RadioButton gender;
        private Enum vrstaRacuna;
        List<BankovniRacun> bankovniRacuni;

        public Banka()
        {
            bankovniRacuni = new List<BankovniRacun>();
            InitializeComponent();
        }

        private void onClearButtonClick(object sender, RoutedEventArgs e)
        {
            this.refresh();

            
        }
        private void refresh()
        {
            this.OIBSearchBox.Text = "";
            this.accountNumberBox.Text = "";
            this.accountTotalBox.Text = "";
            this.accountType.SelectedItem = null;
            this.firstNameBox.Text = "";
            this.lastNameBox.Text = "";
            this.OIBBox.Text = "";
            this.dateInput.SelectedDate = null;
            this.dateInput.DisplayDate = DateTime.Now;
            this.addressBox.Text = "";
            this.maleRadio.IsChecked = false;
            this.femaleRadio.IsChecked = false;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.gender = (RadioButton)sender;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            /*if (this.emptyFieldCheck())
            {
                this.messageLabel.Content = "Sva polja moraju biti popunjena.";
                return;
            }
            if (this.validValuesCheck())
            {
                return;
            }*/

            this.vrstaRacuna = (VrsteRacuna)this.accountType.SelectedIndex;

            if (this.bankovniRacuni.Exists(bankovniRacun => bankovniRacun.Klijent.OIB == this.OIBBox.Text)){
                this.messageLabel.Content = "Klijent sa tim OIB-om već postoji.";
                return;
            }

            Klijent noviKlijent = new Klijent(
                this.firstNameBox.Text, this.lastNameBox.Text,
                this.OIBBox.Text, this.dateInput.SelectedDate,
                this.addressBox.Text, this.gender.Content.ToString().ElementAt(0));



            BankovniRacun bankAccount = new BankovniRacun(
                this.accountNumberBox.Text, double.Parse(this.accountTotalBox.Text),
                (VrsteRacuna)this.vrstaRacuna, noviKlijent);

            this.bankovniRacuni.Add(bankAccount);
            this.messageLabel.Content = "";
            this.refresh();

        }

        private Boolean emptyFieldCheck()
        {
            if (this.accountNumberBox.Text == "")
            {
                return true;
            }
            if (this.accountTotalBox.Text == "")
            {
                return true;
            }
            if (this.accountType.SelectedItem == null)
            {
                return true;
            }
            if (this.firstNameBox.Text == "")
            {
                return true;
            }
            if (this.lastNameBox.Text == "")
            {
                return true;
            }
            if (this.OIBBox.Text == "")
            {
                return true;
            }
            if (this.dateInput.SelectedDate == null)
            {
                return true;
            }
            if (this.addressBox.Text == "")
            {
                return true;
            }
            if (this.gender.IsChecked == false)
            {
                return true;
            }
            return false;
        }

        private Boolean validValuesCheck()
        {
            if (this.accountNumberBox.Text.Length < 12)
            {
                this.messageLabel.Content = "Broj računa je prekratak.";
                return true;
            }
            if (this.accountNumberBox.Text.Length > 12)
            {
                this.messageLabel.Content = "Broj računa je predug.";
                return true;
            }
            try
            {
                double.Parse(this.accountTotalBox.Text);
            }
            catch (Exception e)
            {
                this.messageLabel.Content = "Iznos na računu je neispravan.";
                return true;
            }
            if (!Regex.Match(this.firstNameBox.Text, "^[A-Z][a-zA-Z]*$").Success)
            {
                this.messageLabel.Content = "Neispravan unos imena.";
                return true;
            }

            if (!Regex.Match(this.lastNameBox.Text, "^[A-Z][a-zA-Z]*$").Success)
            {
                this.messageLabel.Content = "Neispravan unos prezimena.";
                return true;
            }
            if (!Regex.Match(this.OIBBox.Text, "^[0-9]{1,11}*$").Success)
            {
                this.messageLabel.Content = "OIB se satoji od 11 znamenki";
                return true;
            }
            if (this.dateInput.SelectedDate.Value.AddYears(18) <= DateTime.Today)
            {
                this.messageLabel.Content = "Osoba mora biti punoljetna";
                return true;
            }
            if (this.addressBox.Text == "") 
            {
                this.messageLabel.Content = "Sva polja moraju biti popunjena";
                return true;
            }
            if (!Regex.Match(this.lastNameBox.Text, "[A-Za-z0-9]+(?:\\s[A-Za-z0-9'_-]+)+$").Success)
            {
                this.messageLabel.Content = "Neispravna adresa.";
                return true;
            }
            return false;
        }

        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {
            if(this.OIBSearchBox.Text == "")
            {
                return;
            }
            /*if (!Regex.Match(this.OIBSearchBox.Text, "^[0-9]{1,11}*$").Success)
            {
                this.messageLabel.Content = "OIB za pretragu je neispravan.";
                return;
            }*/

            BankovniRacun racun = bankovniRacuni.Find(bankovniRacun => bankovniRacun.Klijent.OIB.Equals(this.OIBSearchBox.Text));
            Console.WriteLine(racun);
            this.setValuesByAccount(racun);




        }

        private void setValuesByAccount(BankovniRacun racun)
        {
            this.accountNumberBox.Text = racun.brojRacuna;
            this.accountTotalBox.Text = racun.iznosRacuna.ToString();
            this.accountType.SelectedIndex = (int)((VrsteRacuna)Enum.Parse(typeof(VrsteRacuna), racun.vrstaRacuna));
            this.firstNameBox.Text = racun.Klijent.ime;
            this.lastNameBox.Text = racun.Klijent.prezime;
            this.OIBBox.Text = racun.Klijent.OIB;
            this.dateInput.SelectedDate = racun.Klijent.datumRodenja;
            this.addressBox.Text = racun.Klijent.adresa;
            if (racun.Klijent.spol.Equals('M'))
            {
                this.maleRadio.IsChecked = true;
            }
            else
            {
                this.femaleRadio.IsChecked = true;
            }

        }
    }
}
